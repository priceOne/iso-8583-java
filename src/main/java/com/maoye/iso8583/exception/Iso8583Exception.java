package com.maoye.iso8583.exception;

/**
 * 自定义错误
 * @author pengyong
 * @dataTime 2021年7月9日 下午5:19:52
 */
public class Iso8583Exception extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public Iso8583Exception() {
		super();
	}

	public Iso8583Exception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public Iso8583Exception(String message, Throwable cause) {
		super(message, cause);
	}

	public Iso8583Exception(String message) {
		super(message);
	}

	public Iso8583Exception(Throwable cause) {
		super(cause);
	}
}